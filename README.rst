dj_media_url
============

`dj-database-url <https://github.com/kennethreitz/dj-database-url>`_ for user-uploaded media. A library that lets you conveniently use local files in development and `django-storages <https://django-storages.readthedocs.io>`_ in production, configuring the latter for you.

Stability
---------

Filesystem and S3 URLs are working. There is undocumented, untested support for Google Cloud and Azure also.

URL structure and public API may change before 1.0, so make sure you pin your versions.

Usage
-----

::

    from os import environ
    from dj_media_url import media_url

    media_settings = media_url(environ['USER_MEDIA_URL'])
    globals().update(media_settings)



Setting the media URL
---------------------

Local Files
:::::::::::

Set like ``file:///srv/www/mysite-media`` for absolute paths, or ``file://./media/`` for relative ones.

To set ``MEDIA_URL`` (the URL that users will ultimately access files on), include the ``url`` query parameter, for instance ``file:///srv/www/mysite-media?url=https://media.mysite.com``.

Amazon S3
:::::::::

Set with your Access Key ID, Secret Access Key and bucket name like ``s3://K2ZZSN2EYALQC1OX3ZNI:m+mn1y5EPv1si2+7f5UvpK+buUeZwMpKtLl9DC9U@/my-bucket``.

To write to a subpath within the bucket, add the path to the end of the URL, e.g. ``s3://...@/my-bucket/my-directory/my-subdirectory``.

To use a different S3-compatible service other than Amazon, set the host in the URL, e.g. ``s3://...@my-other-service.com/``.

To use HTTP instead of HTTPS, use the ``s3-insecure:`` protocol instead of ``s3:``.

You can set `other settings <https://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html#settings>`_ using the query string:

========================== ======================== ============================
Setting                    Query String Parameter   Example
========================== ======================== ============================
``AWS_DEFAULT_ACL``        ``default-acl``
``AWS_QUERYSTRING_AUTH``   ``querystring-auth``     ``?querystring-auth=no``
``AWS_QUERYSTRING_EXPIRE`` ``querystring-expire``   ``?querystring-expire=3600``
``AWS_S3_FILE_OVERWRITE``  ``overwrite``            ``?overwrite=no``
``AWS_S3_REGION_NAME``     ``region``
``AWS_S3_CUSTOM_DOMAIN``   ``custom-domain``
========================== ======================== ============================
