from setuptools import setup

setup(
    name='dj-media-url',
    description='My awesome project',
    url='https://example.com/',
    author='Adam Brenecki',
    author_email='adam@brenecki.id.au',
    license='Proprietary',
    setup_requires=["setuptools_scm>=1.11.1"],
    use_scm_version=True,
    py_modules=['dj_media_url'],
    install_requires=[
        'boto3',
        'django-storages',
        'urlobject>=2.4,<3',
    ],
    extras_require={
        'dev': [
            'pytest',
            'pre-commit',
            'prospector',
        ]
    },
)
